diccionario = {'a':55, 5:"Esto es una cadena", (1,2):False} # Las claves deben ser inmutables
diccionario['c'] = "Nueva cadena";                          # Creamos nueva clave:valor
diccionario['a'] = False                                    # Si la clave se encuentra actualiza el valor

#valor = diccionario['a']                                    # Obtenemos el valor de una clave
#valor = diccionario.get('a', 'Esta clave no se encuentra en tu diccionario')

#del diccionario[5]                                          # Eliminar un elemento del diccionario

#print(diccionario)
#print(valor)

llaves  = tuple(diccionario.keys())
valores = tuple(diccionario.values())

print(llaves)
print(valores)