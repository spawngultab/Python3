#!/usr/bin/env python3
#-*- coding: utf-8 -*-

curso = "Curso"
tema  = "Python 3"

""" Métodos de Formato """
resultado = "{} de {}".format(curso, tema)
resultado = "{a} de {b}".format(a = curso, b = tema)
resultado = resultado.lower()
#resultado = resultado.upper()
resultado = resultado.title()

""" Métodos de Búsqueda """
posicion = resultado.find("Curso")
contador = resultado.count("o")

""" Métodos de Sustitución """
nueva_cadena = resultado.replace("o", "x")
nueva_cadena = resultado.split(" ")

print(nueva_cadena)