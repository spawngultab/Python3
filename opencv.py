import cv2

cap = cv2.VideoCapture(0)

while(True):
    # captura de cuadro por cuadro
    ret, frame = cap.read()

    # Convertimos a escala de grises
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Muestra resultado
    cv2.imshow('frame', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
