import requests

if __name__ == 'main':
    url = 'https://www.google.com.mx'
    response = requests.get(url)

    if response.status_code == 200:
        contenido = response.content

        file = open('google.html', 'wb')
        file.write(contenido)
        file.close()
