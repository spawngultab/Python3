#!/usr/bin/env python3
#-*- coding: utf-8 -*-
"""
mi_lista = ["cadenas", 23, 12.25, True]
mi_lista.append(6)
mi_lista.insert(1, "cadena insertada")
mi_lista.remove(23)

print(mi_lista)
ultimo_valor = mi_lista.pop()

print(mi_lista)
print(ultimo_valor)

mi_lista = [1, 9, 22, 6, 8, 65, 14, 99]
print(mi_lista)

mi_lista.sort()
print(mi_lista)

mi_lista.sort(reverse = True)
print(mi_lista)
"""
mi_lista = [1, 9, 22, 6, 8, 65, 14, 99]
mi_lista2 = [2, 15]

mi_lista.extend(mi_lista2)
print(mi_lista)