#!/usr/bin/env python3

my_string = 'Hola Mundo!!! "Oscar"'
my_string = '''Este es un string que contiene\nsaltos de línea.\nAdios!'''

curso  = "Python 3"
nombre = "Oscar"

mensaje_final_1 = "Nuevo Curso de " + curso + " por " + nombre
mensaje_final_2 = "Nuevo Curso de %s por %s" %(curso, nombre)
mensaje_final_3 = "Nuevo Curso de {} por {}".format(curso, nombre)

print(my_string)
print(mensaje_final_1)
print(mensaje_final_2)
print(mensaje_final_3)
