#!/usr/bin/env python3
#-*- coding: utf-8 -*-
"""
lista = []
for valor in range(0, 101):
	lista.append(valor)
"""

"""
Elementos:
1. valor a agregar a la lista
2. ciclo
3. condición

lista = [(1.) valor  (2.) for valor in range(0, 101) (3.) if valor % 2 == 0]
"""

lista       = [valor for valor in range(0, 101) if valor % 2 == 0]
tupla       = tuple((valor for valor in range(0, 101) if valor % 2 != 0))
diccionario = {indice:valor for indice, valor in enumerate(lista)}

#print(lista)
#print(tupla)
print(diccionario)
