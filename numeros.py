#!/usr/bin/env python3

numero_uno = 8
numero_dos = 3

resultado = numero_uno + numero_dos
print("Suma", resultado)

resultado = numero_uno - numero_dos
print("Resta", resultado)

resultado = numero_uno * numero_dos
print("Multiplicación", resultado)

resultado = numero_uno / numero_dos #Devuelve un número flotante
print("División", resultado)

resultado = numero_uno // numero_dos
print("División", resultado)

resultado = numero_uno ** numero_dos
print("Exponencial", resultado)